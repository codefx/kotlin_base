fun main(args: Array<String>) {
    var singleRes=MyStaticClass.multiply(3,5)
    println(singleRes)
    println(Math.min(3,8))

    val prs=object : Student(){
        override fun writeCode() {
            println("yeah, I'm writing an AI")
        }
    }
    prs.eat(); prs.study(); prs.writeCode()
    println(prs.javaClass)
    val prs2=Student()
    println(prs2.javaClass); println("-------**********-------")
    //companion object
    val man= Employee()
    man.func(); man.name
    Employee.age; Employee.calling()

}
//   Object  /ie static
object MyStaticClass {
    var a=0
    var b=1
    fun multiply(x:Int, y:Int) :Int = x * y
}

open class Student{
    fun eat() = println("eating food")
    fun study() = println("studying food")
    open fun writeCode() = println("writing code")
}

//   Companion Object
class Employee{
    var name:String? = null
    companion object{
        var age:Int = 27
        fun calling()= println("calling")
    }
    fun func()= println("this is function.")
}