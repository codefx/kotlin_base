
fun main(args: Array<String>) {
    var t=Teacher("t1",26)
    var s=Soccer("s1",26)
    t.teaching();s.playfootball();

}
//getter setter
class trm{
    var name: String ="wqw"
    get() = field
    set(value) {
        field=value
    }
}
class usr{
    var age:Int =0
    get() = field
    set(value) {
        field= if (value< 18) 18
        else if (value >=18 && value<= 30) value
        else value-3
    }
}

//inheritence  with "open",for override // super methods
open class Persons(var name:String, var age:Int){
    init {
        println("name is : $name")
        println("age is : $age")
    }
    open fun displayWeight(weight:Int){   // override-able with open
        println("weight is $weight")
    }
    open var length:Int? = null
    get() = field
    set(value) {
        field=value
    }

}
class Teacher(tname:String, tage:Int): Persons(tname,tage){
    fun teaching() = println("$name ders veriyor")  // from base class "name" prop
    override fun displayWeight(weight: Int) {//method OVERRIDE
        println("weight is ${weight-10}")
        super.displayWeight(weight)  //SUPER mean from base class
    }

    override var length: Int?=null  // property override
        get() = field  //super.length
        set(value) {
            field = if (value == null) -1 else value
        }

}
class Soccer(sname:String, sage:Int): Persons(sname, sage){
    fun playfootball()=println("$name futbol oynuyor")
}

