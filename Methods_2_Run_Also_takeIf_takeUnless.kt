import java.nio.file.Paths

fun main(args: Array<String>) {
    //---run---- let and with methods merged version
    /*val outputPath=Paths.get("/user/home").run {
        val path=resolve("output")
        path.toFile().createNewFile()
        path
    }*/

    // ---- also --- in addition that also these
    val address=Address("new",35100)
    val userq=UserQ("CFX",address)
    val res=userq.also { it.age=40 } // {t -> t.age=40}

    // -- takeIf -- if true returns the object's self,
    // if false  return null or "String" // write any condition no matter
    var q=userq.takeIf { it is UserQ } ?: "not an userQ" // null
    println(q)
    //--- takeUnless---- opposite takeIf,
    var q2=userq.takeUnless { it is UserQ } ?: " an userQ" // null
    println(q2)
}

data class Address(var street:String?=null, var code:Int?=null)
data class UserQ(var name:String?=null, var address:Address?=null, var age:Int?=null)