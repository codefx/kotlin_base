fun main(args: Array<String>) {
    var light =Light()
    light.turnOn()
    light.turnOff()

    var user=User("codefx",38)
}
class OOPs {
}
public class Light{
    var isOn: Boolean =false
    lateinit var abc:String
    //lateinit var tbc:Int  /*primitive type not allowed*/
    fun turnOn(){
        isOn=true
    }
    fun turnOff() {
        isOn =false
    }
}

private class PrivClass{}
internal class InternClass{}

open class A{}  //inheritence "open"
class B : A (){}

class Person{
    var _name: String? = null
    var _age: Int? = null
    var prop: String?=null
    constructor(name:String, age:Int, prop : String){
        _name=name  //_name for this.name !!
        _age=age
        this.prop=prop
    }
}
//with constructor and properties---vvv
// only with constructor ..(name:String, age:Int)
// with properties and constructor ..(var name:String, var age:Int)
class Person2(var name:String?=null, var age:Int?=null, val prop : String?=null){

}

class User(firstName:String, userAge:Int){
    var name:String?=null
    var age:Int?=null

    init {
        name=firstName.capitalize()
        age=userAge
        println("firstname is $name, userage is $age")
    }
}
