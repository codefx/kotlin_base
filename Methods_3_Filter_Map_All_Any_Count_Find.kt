fun main(args: Array<String>) {

    // this funcs with "{" .. "}"
    var list= listOf(1,2,3,4,5,6,7,8,9,10,11,12)
    //filter --> only has condition
    var list2=list.filter { it> 10 }
    list2.printList()
    // map --> every one of them do the condition
    var l3=list.map {it * 2} // {i -> i*2}
    l3.printList()
    //all --> all of them,if all meet the condition, Returns true .
    var q=list.all { it> 5 }; println(q) // true or false
    //any --> any of them,if any of it, meet the condition, Returns true .
    var m=list.any { it> 8 }; println(m) // true or false
    //count --> Returns the number of elements that meet the condition
    var n=list.count { it> 6 }; println(n)
    //find --> Returns the first element that meets the condition
    var r=list.find{ it> 4}; println(r) // 5


}
fun<T> List<T>.printList(){
    forEach { println(it) }
}