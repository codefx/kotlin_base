fun main(args: Array<String>) {
    // all types inherit from "Any" type in kotlin
    // cast ??? smart cast, ? or !! null safety
    //platform types -> from java types
    var a: Any =2; var b:Any="deneme"
    var c:Float = (a as Int).toFloat()  // cast with "as" or "as?"
    when(b){
        is Int -> println(b)
        is Float -> println(b)
        is String -> println(b)
    }
    println(c)
    var hpr= mapOf(1 to "q",2 to "w")
    for((key, value) in hpr){
        println("key : $key - value: $value")
    }
    var qwq=Prof("One")
    var qwq2=Prof("One","Two")
    var qwq3=Prof("One","two", 3)
   /*1.*/ Outers.Account().writeFirstName()
   //*2.*/ Outers("one","two").Account().writeFirstName() //if u use "inner"
    println(qnamed("qq",{a-> a+30}))
    println(qnamed("qq") {a-> a+27})
    println(qnamed("qq") {a->
        var b=24
        return@qnamed b+a
    })
    println(qnamed("qq") {
        var b=24
        return@qnamed b+it
    })

// func are seperated "root level" and "member" func (ie. methods)
}
class Profile
//running queue 1-primary constructor- 2-every init block- 3-secondary or other constructors.
class Prof /*constructor*/(fisrt:String){  //primary constructor
    init {
        println(fisrt) // this  variable from primary constructor
    }
    constructor(fisrt: String, last:String) :this(fisrt) { /*secondary constructor must be call first cons.*/
    println("$fisrt, $last")
    }
    init {
        println("second init block")
    }
    constructor(fisrt: String, last:String, age:Int) :this(fisrt,last) { /*at least one constructor must be call .*/
        println("$fisrt, $last, $age")
    }
}

//nested classes
class Outers(var firstname:String,var lastName:String){
    private var mfirstName:String=firstname
    /*inner */class Account{  //if you wish accessing the outer class's members use "inner"
        fun writeFirstName(){
            println("mrb") //;println(mfirstName);
        }
    }
}
// bir fonsiyonun son parametresi bir fonsiyon ise bunu dışına çıkararak yazabilirsiniz.
fun qnamed(fn:String,age:(Int)->Int):String{
    return "Hi, $fn, ${age(2)}"
}

