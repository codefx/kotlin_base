fun main(args: Array<String>) {
    var a:String? ="Null  safety"
    println(a?.length)  //nullable da alt metodlara erişebilmek için ? gelmeli.
    //buna null safety deniyor. bu güvenli yol

    val b= a!!.length   // bu sonucuna katlanırım demek. güvensiz yol !!

    var t=11;  var y=9
    var maxy = if (t>y) t else y

    var wr=when(t){
        0,1,2,3,4,5 -> println("0, 1, 2, ,3,4,5 ten biri")
        6,7 ->{
            println("6 olabilir")
            println("7 de olabilir")
        }
        in 8..10 -> println("8 ila 11 arası")
        !in 0..12 -> println("12 den buyuk")
        else -> println("11 olabilir")
        }
    println("////////////*********************////")
    for (i in a.indices) println(a[i]) //
    for (i in 7 downTo 2) println(i)  //azalan aralıkta downTo
    for (i in 7 downTo 1 step 2) println(i)  //azalan aralıkta downTo
    var z=1
    "qwerty".forEach { i -> println("$i,${z++}") }
    (12 downTo 3 step 2).forEachIndexed { index, i -> println("$index, $i") }
    "Kotlin language".forEachIndexed { k, v -> println("$k, $v") }
    //readingWhile()
}
fun readingWhile(){
    var name:String
    var passw:String
    do {
        println("enter your name : ")
        name= readLine().toString().toLowerCase()
        println("enter your password : ")
        passw= readLine().toString().toLowerCase()
    }while (name!="codefx" && passw!="cfx")
    println("enterence successful")

}