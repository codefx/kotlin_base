fun main(args: Array<String>) {
    val perBeing=Being("Codefx",39)
    println("name is ${perBeing.name}, age is ${perBeing.age}")
    println("(Component)name is ${perBeing.component1()}, age is ${perBeing.component2()}")

    println("*********/////***********")
    val perBeing2 = perBeing.copy("CFX")
    println("name is ${perBeing2.name}, age is ${perBeing2.age}")
    println(perBeing2.toString())
    val perBeing3=perBeing2.copy(); println("*********/////***********")

    println("perbeing hashcode is ${perBeing.hashCode()}")
    println("perbeing2 hashcode is ${perBeing2.hashCode()}")
    println("perbeing3 hashcode is ${perBeing3.hashCode()}")
    println("*********/////***********")
    
    if (perBeing.equals(perBeing2)) println("perBeing is equal to perBeing2")
    else println("perBeing is NOT equal to perBeing2")
    if (perBeing2.equals(perBeing3)) println("perBeing2 is equal to perBeing3")
    else println("perBeing2 is NOT equal to perBeing3")

}

// one line all data, all data class have toString(), hashcode() and equals() methods
data class Being(var name:String, var age:Int)