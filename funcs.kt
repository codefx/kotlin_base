fun main(args: Array<String>) {
    hlw()
    println(msgwrite("one line funcs"))
    println(4.triple())
    var list= arrayListOf<String>("Kotlin","Android","Python", "java", "C#")
    list.forEach { i -> println(i)}
    list.swap(0,3)
    println("-------------------------")
    list.forEach { i -> println(i)}
    var tt= 5 powerit 3 // infix func
    var ty= 5.powerit(4)  // infix but already ext funcs ;)
    var tz= "Kotlin" concat "is Amazing"  //infix
    println("tt is $tt, ty is $ty \n tz is $tz")
    println("Kotlin".concat("is awesome")) // ext

}


fun hlw()= println("hello world")
fun msgwrite(message:String):String ="your msg is : $message"
fun calc(num1:Int, num2:Int, process:String):Int = when(process){
    "+" -> num1 + num2
    "-" -> num1 - num2
    "*" -> num1 * num2
    "/" -> num1 / num2
    else -> 0
}

//EXTENSION FUNCS

fun Int.triple()= this * 3

fun ArrayList<String>.swap(to:Int, from:Int){
    val temp = this[to]
    this[to] = this[from]
    this[from]= temp
}

//INFIX FUNCS--> all infix are ext funcs
// all ext func ARE NOT infix funcs
// infix funcs can get only one argument

infix fun Int.powerit(pow:Int): Int= this * pow   // "this" is the before argument
infix fun String.concat(other:String) = this + " "+ other