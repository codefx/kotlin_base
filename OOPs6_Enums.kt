fun main(args: Array<String>) {
    println(calcWithDay(amount=100))
    println(Colors.GREEN)
    println(Colors.GREEN.rgb)
    println(Colors.GREEN.name)
    println(daysName(Days.PERSMB))


}

fun calcWithDay(gun:Days=Days.PTS, amount:Int) :Int=
     when(gun){
         Days.PTS -> amount * 2
         Days.SALI -> amount * 3
         Days.CARSMB -> amount - 2
         Days.PERSMB -> amount + 2
         Days.CUMA -> amount * 5
         Days.CTS -> amount -3
         Days.PZR -> amount -1
     }
fun daysName(day:Days):String{
    return day.getformattedName()
}
enum class Days{
    PTS, SALI, CARSMB, PERSMB, CUMA, CTS, PZR;
    fun getformattedName()=name.toLowerCase().capitalize()
}

enum class Colors(val rgb:Int){
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF);
}