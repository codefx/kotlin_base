fun main(args: Array<String>) {

    val InterstingThings= listOf("Kotlin","Android","Java","Python")
    val IntrstngThngs= listOf("Kotlin","Android","Java","Python",null,null)
    val InterstingThings2= arrayOf("Kotlin","Android","Java","Python")
    saySomething("Hi", "Kotlin","C#","SQL","Docker")
    saySomething("Hi", *InterstingThings2)
    printInfo(null)
    val entity=Entity.Factory.create()
    //hof
    InterstingThings
        .filterNotNull()
        .filter { it.startsWith("J") }
        .takeLast(3)
        .map { it.length }
        .forEach { println(it) }
    IntrstngThngs
        .filterNotNull()
        .take(3)
        .associate { it to it.length }
        .forEach { println("${it.value}, ${it.key}")  }
}

//vararg ?? like  arg kwarg in pyt, spread operator  --> "*" and variable must be "array"

fun saySomething(greeting:String,vararg itemstoGreet:String){
    itemstoGreet.forEach { i-> println("$greeting, $i") }
}
// elvis operator
fun printInfo(nickname:String?){
    val nicknameToPrint= nickname ?: "No nickname"
    println(nicknameToPrint)
    println(nickname ?: "No nickname")
}
// object using, creating anonymous inner class.. (01:43:50)
interface IdProvider{ fun getId():String}
class Entity private constructor(val id:String){

    companion object Factory : IdProvider{

        override fun getId(): String {
            return "123"
        }
        const val id="id"
        fun create()=Entity(getId())
    }
}

//hof