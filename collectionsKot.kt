fun main(args: Array<String>) {
   // Listers()
    //mappers()
    setters()
}
/*
* Collections
*   Immutable /readonly
*       1. List -> listOf
*       2. Map -> mapOf
*       3. Set -> setOf
*   Mutable /  read /write - able
*       1. List -> ArrayList, arrayListOf,mutableListOf
*       2. Map -> HashMap, hashMapOf, mutableMapOf (LinkedHashMap)
*       3. Set -> mutableSetOf, hashSetOf
* */

//List
fun Listers(){
    var list= listOf<String>("Kotlin","Android","Python", "java", "C#")
    //list[2]="abc"  --> not possible
    list.forEach { i -> println(i)};println("*********************")

    /************ mutable list funcs ****************/ //
    var list2= mutableListOf<String>("Kotlin","Android","Python", "java", "C#")
    list2[2]="abc"; list2.add("React");
    list2.remove("java");
    list2.removeAt(list2.size - 1)
    list2.forEach { i -> println(i)}

    var listArrlistOf= arrayListOf<String>("ister burada ister add ile")
    var listArrList= ArrayList<String>()
    listArrList.add("ArrayList  e sadece bu şekilde eklenebilir")
    list2.forEach { i -> listArrList.add(i)}
    println(listArrList.toString()); println("-----------")
    list.forEach { i ->listArrlistOf.add(i) };println(listArrlistOf.toString())

}

fun mappers(){
    var map1=mapOf<Int,String>() // burada tanımlarsan olur, diğer türlü hep boş
    var x=map1.size; println(x)
    var map2=mapOf<Int,String>(8 to "abc",11 to "def", 17 to "mnr")
    for (key in map2.keys) println("Key is $key, value is ${map2[key]}")
    println("--------*-*-*-*-*-*----------")
    map2.forEach { k, v -> println("Key is $k, value is $v") }
    /********Mutable map funcs *********************/
    var map3=HashMap<Int,String>()
    map3.put(11,"wwq");map3.put(24,"wwr");map3.put(16,"qwq");map3.put(22,"rrt")
    map3.forEach { k, v -> println("Key is $k, value is $v") };println("-----map3----")
    map3.replace(16,"YYW")
    map3.forEach { k, v -> println("Key is $k, value is $v") };println("-----map3----")
    map3.remove(22)
    map3.forEach { k, v -> println("Key is $k, value is $v") };println("-----map3----")
    var map4= hashMapOf<Int,String>(11 to "awwq",9 to "bttr",21 to "dqq2",13 to "cgge" )
    var map5= mutableMapOf<Int,String>(11 to "bwwq",9 to "attr",21 to "dqq2",13 to "cgge" )
    map4.forEach { k, v -> println("Key is $k, value is $v") };println("-----map4----")
    map4.toSortedMap().forEach { k, v -> println("Key is $k, value is $v") };println("-----map4-sorted----")
    map5.forEach { k, v -> println("Key is $k, value is $v") };println("-----map5----")
    for (key in map5.keys) println("Key is $key, value is ${map5[key]}")




}

fun setters(){
    var set1= setOf<Int>(3,3,2,2,1,5,9,9,4,8,7,6)
    println(set1.size)

    var set2= mutableSetOf<Int>(3,3,2,2,1,5,9,9,4,8,7,6)
    set2.add(10);set2.add(4)
    set2.forEach { i -> println(i) };println()
    set2.remove(6)
    set2.forEach { i -> println(i) };println()
    var set3= hashSetOf<Int>(3,3,2,2,1,5,9,9,4,8,7,6)
    set3.forEach { i -> println(i) };println()

}