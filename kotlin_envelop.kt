fun main() {
    repSquare()
    squareMaker("W",9)
    SM("*",11)

}

fun repSquare(){
    repeat(10){ x->
        repeat(10){ y->
            var text= if(x==0 || x==9 || y==0 || y==9 || x == y || x+y == 9) "# " else "  "
            print(text)
        }
        println()
    }
}

fun squareMaker(symbol: String, bound:Int){
    repeat(bound){ x->
        repeat(bound){ y->
            val text= if(x==0 || x==(bound-1) || y==0 || y==(bound-1) || x==y || x+y==(bound-1)) "${symbol} " else "  "
            print(text)
        }
        println()
    }
}

fun SM(symbol: String, bound:Int){
    for(x in 0..bound){
        for(y in 0..bound){
            val text= if(x==0 || x==(bound) || y==0 || y==(bound) || x==y || x+y==(bound)) "${symbol} " else "  "
            print(text)
        }
        println()
    }
}