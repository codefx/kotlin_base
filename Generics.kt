fun main(args: Array<String>) {
    var gen1= Gen1<Int>()
    gen1.pls(3)
    var gen2= Gen1<String>()
    gen2.get()
    var gen3= Gen2<ScnClass>(); gen3.genFunc(ScnClass())
    // var gen4=Gen2<ThrClass>() not from FrsClass
    prlRepeater("Word",6)
    prlRepeater(true,4) ; prlRepeater(1,4)
    var arr1= arrayOf(1,2,3,4,5); var arr2= arrayOf(2.2, 3.5, 5.1, 8.3)
    var arr3= arrayOf("Kotlin","Android","Python", "java", "C#")
    prlArray(arr1); prlArray(arr2); prlArray(arr3); println("-------------")
    arr1.prlArrayExt(); arr2.prlArrayExt(); arr3.prlArrayExt()
    var res=findBig(2,3,1)
    println(res)


}

class Gen1<T> {
    var _x:T? = null
    fun pls(x:T){ _x= x }
    fun get(): T? =_x
}

open class FrsClass{ fun frs(){} }
class ScnClass: FrsClass(){ fun scn(){} }
class ThrClass /* :FrsClass()*/{ fun thr(){} }

class Gen2<F: FrsClass> { fun genFunc(f: F){} }

// in & out
class Producer<out Y>(val value: Y){
    fun gety():Y =value  // outing Y -> out
    /*fun toString(value: Y): String { // as a parameter Y -> in
        return  value.toString()       // if activate this section you get exception
    }*/                                // del the "out"
}

class Saler<in S>{
    fun toString(value: S): String { // as a parameter S -> in
        return  value.toString()
    }
}

//****** generic Funcs  ********************/

fun <Z> prlRepeater(z: Z, c:Int) {
    (0..c).forEach { i -> println(z) }
    //for (i in 0..c) println(z)
}
fun <W> prlArray(w_array: Array<W>){
    w_array.forEach { i -> println(i) }
    //for (items in w_array) println(items)
}
// generic extended
fun <U> Array<U>.prlArrayExt(){
    this.forEach { i -> println(i) }
    //for (items in this) println(items) // this is Array<U>
}
fun <T: Comparable<T>> findBig(x:T,y:T,z:T) :T {
    var big=x
    if (y.compareTo(big)>0) big=y
    if (z.compareTo(big)>0) big=z
    return big
}