fun main(args: Array<String>) {
    var point = Point(3,-5)
    ++point
    println("${point.x}, ${point.y} \n")

    var point2 = Point2(3,-5)
    point2++
    println("${point2.x}, ${point2.y} \n")
    /*********************/
    val cust=Custom(2,3)
    val cust2=Custom(3,4)
    val result= cust + cust2
    val r2= cust.plus(cust2)
    println("this is result : $result \n this is r2 : $r2 \n")
    val cust3=Custom2(2,3)
    val cust4=Custom2(3,4)
    println("${cust3.plus(cust4)}")

}
/* https://kotlinlang.org/docs/reference/operator-overloading.html
        a++ 	a.inc()  --> inc()
        a-- 	a.dec()
        a + b 	a.plus(b) --> plus()
        a - b 	a.minus(b)
        a * b 	a.times(b)
        a / b 	a.div(b)
        a % b 	a.rem(b), a.mod(b) (deprecated)
        a..b 	a.rangeTo(b)
        ---------------------------
        a += b 	a.plusAssign(b)
        a -= b 	a.minusAssign(b)
        a *= b 	a.timesAssign(b)
        a /= b 	a.divAssign(b)
        a %= b 	a.remAssign(b), a.modAssign(b) (deprecated)
        ---------------------------------
        a > b 	a.compareTo(b) > 0
        a < b 	a.compareTo(b) < 0
        a >= b 	a.compareTo(b) >= 0
        a <= b 	a.compareTo(b) <= 0
*/

class Point(var x:Int=0, var y:Int=5){
    operator fun inc()=Point(++x,++y) //++ -> inc()
}
class Point2(var x:Int=0, var y:Int=5){
    operator fun inc() =Point2(x*2,y*2) //++ -> inc()
}

data class Custom(val pay:Int, val payda:Int){
    operator fun plus(new_cust:Custom) : Custom =
        if (this.payda == new_cust.payda)  //this the class's payda
            Custom(this.pay + new_cust.pay, this.payda)
        else
            Custom(this.pay * new_cust.payda+ new_cust.pay* this.payda,
            this.payda* new_cust.payda)
}
data class Custom2(val pay:Int, val payda:Int) {
    operator fun plus(new_cust: Custom2): Custom2 = Custom2(pay * 2 + new_cust.pay,
        payda * 2 + new_cust.payda)
}