fun main(args: Array<String>) {
    sum(3,4)
    sum2(4,5)
    bigger(6,3)
    bigger2(3,6)
    myLambdaFunc("cfx","codefx")
    //************************************/// HighOrderFuncs
    callIt { println("calling funcs") }
    sayingThings("Codefx",{ println(it)})   // {name -> println(name)}
    withType("codefx", { it.reversed() })
    useBlck { func1(); func2(); func3(); prop}
}
///***** Lambda  { } unnnamed funcs.***///
fun sum(x:Int, y:Int):Int = x + y
val sum2= {x:Int,y:Int -> x+y}

fun bigger(x:Int,y:Int):Int = if (x > y) x else y
var bigger2={x:Int,y:Int -> if (x > y) x else y } // parameter type must be

var myLambdaFunc:(String,String) -> Unit= {ad,soyad -> println("$ad, $soyad")}

var t=(1..10).forEach{ i -> println(i)} // or
var t2=(1..10).forEach{ println("t2 is $it")} // single parameter lambda func-> "it" -erator

// High Order Funcs--> contains or usings lambda funcs
//basic --v
fun callIt(func: ()-> Unit){
    func()
}
fun sayingThings(name:String, messageBody:(String) -> Unit){
    messageBody("Hi, $name")  // in main func using that --v
}                               //sayingThings("Codefx",{ println(it)}) single param lambda

fun withType(str:String, func:(String) ->String){
    println(func(str))
}
// High Important
class Blck{
    var prop:String?=null
    fun func1()= println("func1")
    fun func2()= println("func2")
    fun func3()= println("func3")
}

fun useBlck(block: Blck.() -> Unit){
    var b= Blck()
    b.block()
}