fun main(args: Array<String>) {
    val cfx =Worker("Codefx")
    cfx.showAge(38);cfx.jobIs("developer")
    var z=Z()
    z.hello(); z.tFunc()
}

//*     ABSTRACT     */
// can not create an instance of an abstract class
// if in a class, there is an abstract func, the class must be abstract
// an abstract class is inheritable from an other abstract class.

abstract class Human(name:String){
    init {
        println("name is $name")
    }

    fun showAge(age: Int){
        println("age is $age")
    }
    abstract fun jobIs(job: String) // no body
}

class Worker(ad:String):Human(ad){
    override fun jobIs(job: String) {
       println("job is $job")
    }

}

abstract class Shape(var height:Int, var width:Int){
    abstract fun calcArea(height:Int, width:Int)
}
abstract class Rectangle( height:Int, width:Int):Shape(height, width)

//*   INTERFACE   */

interface MyInterface{
    var prop:String
    fun Anyof()
    fun Hi()="Hi !!!"
}

class ImpleInter:MyInterface{
    override var prop: String="Kotlin"
    override fun Anyof() {
    }
}

// Multiple , conflict and nested interface
interface  X {fun hello()= println("Hello from X")}
interface  T {fun tFunc()}
interface  Y: T {fun hello()= println("Hello from Y")}
interface  C { interface D { fun dFunc() }}
class Z : X, Y, C.D {
    override fun hello() { //same methods conflicting?
        super<X>.hello()
        super<Y>.hello()
    }
    override fun tFunc() {}
    override fun dFunc() {}
}

