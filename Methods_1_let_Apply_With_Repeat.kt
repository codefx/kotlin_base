fun main(args: Array<String>) {

    // let --> "param is null ?" 's check
    var str: String? = "Codefx"
    var w = if (str != null) {/*not null part*/} else {/*null part*/}

    var _Let = str?.let {
        // not  null, part
        println("not null") //println(it)
        27  //let's return value
    } ?: { //<-- ?: is else part
        // null, part
        println("null")
    }
    println(_Let)
    //--Apply---
    var dev=Developer(); var dev2=Developer()
    dev.apply { // like C# --> new Class(){a=4, B="dfd"} initialize
        age=38
        name="codefx"
        writeCode("Kotlin ")
    }
    //---with----
    with(dev2){
        name="CFX"
        age=38
        writeCode("AndroKotlin")
    }

    //--repeat--
    repeat(6,{repeating("Codefx")})
}

fun repeating(s:String)= println(" word $s")

class  Developer{
    var name:String? =null
    var age:Int? =null
    fun writeCode(t:String)= println(" $t code is written")
}

